let midiPorts = [];
let selectedMidiPort = 0;

let colors = {
    0: "#000000", 1: "#212121", 2: "#8b8b8b", 3: "#ffffff", 4: "#ff6457", 5: "#ff2900", 6: "#6e0a00", 7: "#220100",
    8: "#ffc572", 9: "#ff6c00", 10: "#6e2400", 11: "#2f1a00", 12: "#fbf725", 13: "#fbf700", 14: "#686600", 15: "#1a1900",
    16: "#8bf634", 17: "#3ff500", 18: "#146500", 19: "#103400", 20: "#30f534", 21: "#00f500", 22: "#006500", 23: "#001900",
    24: "#30f657", 25: "#00f500", 26: "#006500", 27: "#001900", 28: "#30f68f", 29: "#00f546", 30: "#006519", 31: "#002012",
    32: "#27f7bd", 33: "#00f7a2", 34: "#00663b", 35: "#001912", 36: "#3fcaff", 37: "#00b6ff", 38: "#004d63", 39: "#00121a",
    40: "#5098ff", 41: "#006cff", 42: "#00266d", 43: "#000421", 44: "#5864ff", 45: "#0433ff", 46: "#01106d", 47: "#000221",
    48: "#9665ff", 49: "#6435ff", 50: "#1b1277", 51: "#0a0641", 52: "#ff6aff", 53: "#ff40ff", 54: "#6e166d", 55: "#220321",
    56: "#ff6694", 57: "#ff2b62", 58: "#6e0b21", 59: "#290212", 60: "#ff3400", 61: "#ad4400", 62: "#8c6200", 63: "#4b7500",
    64: "#004500", 65: "#006141", 66: "#006490", 67: "#0433ff", 68: "#00525d", 69: "#232adb", 70: "#8b8b8b", 71: "#282828",
    72: "#ff2900", 73: "#c4f600", 74: "#b7eb00", 75: "#60f600", 76: "#009500", 77: "#00f68b", 78: "#00b6ff", 79: "#033dff",
    80: "#4333ff", 81: "#8836ff", 82: "#c33090", 83: "#532900", 84: "#ff5e00", 85: "#91e200", 86: "#71f600", 87: "#00f500",
    88: "#00f500", 89: "#47f672", 90: "#00f8d2", 91: "#6199ff", 92: "#2d64d2", 93: "#9590ef", 94: "#dd3fff", 95: "#ff2c6d",
    96: "#ff9100", 97: "#c6ba00", 98: "#95f600", 99: "#956c00", 100: "#473500", 101: "#005b02", 102: "#006147", 103: "#121435",
    104: "#122c6d", 105: "#7d4d19", 106: "#bb1800", 107: "#e96740", 108: "#e57d00", 109: "#ffe400", 110: "#a4e200", 111: "#6fbd00",
    112: "#21223b", 113: "#e2f762", 114: "#81f8c1", 115: "#a7aaff", 116: "#9a7bff", 117: "#4d4d4d", 118: "#868686", 119: "#e2fbfb",
    120: "#b21700", 121: "#420300", 122: "#00d100", 123: "#004b00", 124: "#c6ba00", 125: "#4d3b00", 126: "#c36e00", 127: "#591c00"
}

function sendOutput(note, velocity) {
    midiPorts[selectedMidiPort].send([144, note, velocity]);
}

function clearLaunchpad() {
    for (let i = 0; i < 99; i++)
        sendOutput(i, 0);
}

function getPadID(x, y) {
    x = 1 + x;
    y = (7 - y) + 1;
    return y.toString() + x.toString();
}

function playAnimation(a, endCallback) {
    let interval = setInterval(playFrame, 20);

    if (!a.playing)
        a.playing = true;
    else {
        a.index = 0;
        clearInterval(interval);
        let index = 0;
        while (index < a.data.length - 1) {
            for (let x of Object.keys(a.data[index]))
                sendOutput(x, 0);
            index++;
        }
    }

    function playFrame() {
        if (a.playing) {
            for (let x of Object.keys(a.data[a.index]))
                sendOutput(x, a.data[a.index][x]);

            a.index++;

            if (a.index > a.data.length - 1) {
                a.index = 0;
                a.playing = false;
                clearLaunchpad();
                clearInterval(interval);
                endCallback();
            }
        }
    }

    playFrame();
}

function startMidi(deviceCallback, inputCallback, deviceChangeCallback, errorCallback) {
    navigator.requestMIDIAccess({ sysex: false })
        .then(midi => {
            midi.addEventListener("statechange", deviceChangeCallback);

            let inputs = midi.inputs.values();
            let outputs = midi.outputs.values();

            for (let input = inputs.next(); input && !input.done; input = inputs.next())
                if (input.value.name.toLowerCase().includes("launchpad"))
                    input.value.addEventListener("midimessage", inputCallback);

            for (let output = outputs.next(); output && !output.done; output = outputs.next())
                if (output.value.name.toLowerCase().includes("launchpad"))
                    midiPorts.push(output.value);
            
            deviceCallback(midiPorts);
        }, errorCallback);
}