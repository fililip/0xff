window.addEventListener("load", () => {
    let animation =
    {
        playing: false,
        index: 0,
        data: [
            {
                60: 3
            }, {
                60: 3,
                61: 3
            }, {
                60: 2,
                61: 3,
                62: 3
            }, {
                60: 1,
                61: 2,
                62: 3,
                63: 3
            }, {
                60: 0,
                61: 1,
                62: 2,
                63: 3
            }, {
                60: 0,
                61: 0,
                62: 1,
                63: 2
            }, {
                60: 0,
                61: 0,
                62: 0,
                63: 1
            }, {
                60: 0,
                61: 0,
                62: 0,
                63: 0
            }
        ]
    };

    const Mode = {
        IDLE: 0,
        TOGGLE: 1,
        DRAW: 2
    };

    let frames = [];
    let frame = 0;

    let previousFrame = {};
    let currentFrame = {};
    let mode = Mode.IDLE;
    let currentColor = 0;

    let resetCanvasButton = document.querySelector("#reset");
    let canvas = document.querySelector("canvas");
    let ctx = canvas.getContext("2d");
    let mouse = {
        x: 0,
        y: 0
    };
    let leftMousePressed = false;
    let rightMousePressed = false;

    let logo = document.querySelector("#logo");

    let colorPicker = document.querySelector(".color-picker");

    let addFrameButton = document.querySelector("#add-frame");
    let duplicateFrameButton = document.querySelector("#duplicate-frame");
    let removeFrameButton = document.querySelector("#remove-frame");
    let playAnimationButton = document.querySelector("#play-animation");

    function clearArray() {
        for (let i = 0; i < 99; i++) {
            previousFrame[i] = 0;
            currentFrame[i] = 0;
        }
    }

    function reset() {
        clearArray();
        clearLaunchpad();
    }

    function onMidiInput(message) {
        let note = message.data[1];
        let velocity = message.data[2];

        if (mode == Mode.IDLE && velocity > 0) {
            if (note.toString().length == 2) {
                if (note.toString()[1] == "9") {
                    let id = parseInt(note.toString()[0]);

                    switch (id) {
                        case 1:

                            break;
                        case 2:
                            
                            break;
                        case 3:
                            
                            break;
                        case 4:

                            break;
                        case 5:
                            
                            break;
                        case 6:
                            
                            break;
                        case 7:
                    
                            break;
                        case 8:
                            animation.data = frames;
                            playAnimation(animation, () => {
                                sendFrameToLaunchpad();
                                if (mode == Mode.IDLE)
                                    updateIdle();
                            });
                            document.querySelector("audio").play();
                            break;
                    }

                    return;
                }

                if (note.toString()[0] == "9") {
                    let id = parseInt(note.toString()[1]);

                    switch (id) {
                        case 1:
                            setColor(getCurrentColorIndex() - 1);
                            sendOutput(91, currentColor);
                            sendOutput(92, currentColor);
                            break;
                        case 2:
                            setColor(getCurrentColorIndex() + 1);
                            sendOutput(91, currentColor);
                            sendOutput(92, currentColor);
                            break;
                        case 3:
                            backwardFrame();
                            updateIdle();
                            break;
                        case 4:
                            forwardFrame();
                            updateIdle();
                            break;
                    }

                    return;
                }

                if (parseInt(note.toString()[0]) >= 1 &&
                    parseInt(note.toString()[0]) <= 8 &&
                    parseInt(note.toString()[1]) >= 1 &&
                    parseInt(note.toString()[1]) <= 8) {
                    
                    currentFrame[note] = currentColor;
                    sendOutput(note, currentFrame[note]);

                    return;
                }

                switch (note) {
                    case 10:
                        addFrame();
                        updateIdle();
                        break;
                    case 30:
                        duplicateFrame();
                        updateIdle();
                        break;
                    case 50:
                        removeFrame();
                        updateIdle();
                        break;
                }
            }

            return;
        }

        if (velocity > 0)
            velocity = currentColor;

        switch (mode) {
            case Mode.DRAW:
                currentFrame[note] = currentColor;
                sendOutput(note, currentFrame[note]);
                break;
            case Mode.TOGGLE:
                if (velocity != previousFrame[note]) {
                    if (velocity > 0) {
                        if (currentFrame[note] > 0) {
                            currentFrame[note] = 0;
                            previousFrame[note] = velocity;
                            sendOutput(note, currentFrame[note]);
                            return;
                        } else {
                            currentFrame[note] = velocity;
                            previousFrame[note] = velocity;
                            sendOutput(note, currentFrame[note]);
                            return;
                        }
                    }
                    previousFrame[note] = velocity;
                }
                break;
        }
    }

    function onDeviceChange(device) {

    }

    function onDevicesDetected(devices) {
        if (devices.length <= 0) {
            onNoDevicesFound();
            return;
        }
        
        let deviceList = document.querySelector(".devices");

        for (let i = 0; i < devices.length; i++) {
            let x = devices[i];

            let device = document.createElement("div");

            device.classList.add("device");
            device.innerText = `${x.name} ${(x.manufacturer.length > 0) ? `(${x.manufacturer})` : ""}`;
            device.setAttribute("index", i);

            if (x.name.toLowerCase().includes("midi 2")) {
                selectedMidiPort = parseInt(device.getAttribute("index"));
                device.classList.add("device-selected");
                x.open();
            }

            device.addEventListener("click", () => {
                for (let y of devices)
                    y.close();

                selectedMidiPort = parseInt(device.getAttribute("index"));
                devices[device.getAttribute("index")].open();

                for (let y of document.querySelectorAll(".device")) {
                    y.classList.remove("device-selected");
                    if (y == device)
                        y.classList.add("device-selected");
                }
            });

            deviceList.appendChild(device);
        }

        changeMode(Mode.IDLE);
    }

    function onNoDevicesFound() {
        let deviceList = document.querySelector(".devices");
        deviceList.style.color = "#dd2222";
        deviceList.style.textShadow = "0px 0px 4px #dd2222";
        deviceList.style.padding = "8px";
        deviceList.style.paddingBottom = "0px";
        deviceList.innerText = "No MIDI-capable devices have been found";
    }

    function sendFrameToLaunchpad() {
        for (let i = 0; i < 99; i++) {
            if (mode == Mode.IDLE) {
                if (i.toString().length == 2)
                    if (i.toString()[1] == "9" || i.toString()[0] == "9" ||
                        i.toString()[1] == "0")
                        continue;
            }
            sendOutput(i, currentFrame[i]);
        }
    }

    function updateIdle() {
        sendOutput(91, currentColor);
        sendOutput(92, currentColor);

        if (frames.length > 1) {
            if (frame == 0) {
                sendOutput(93, 0);
                sendOutput(94, 3);
            }

            if (frame > 0 && frame < frames.length - 1) {
                sendOutput(93, 3);
                sendOutput(94, 3);
            }

            if (frame == frames.length - 1) {
                sendOutput(94, 0);
                sendOutput(93, 3);
            }
        } else {
            sendOutput(93, 0);
            sendOutput(94, 0);
        }

        sendOutput(50, 5);
        sendOutput(30, 3);
        sendOutput(10, 29);
        sendOutput(89, 29);
    }

    startMidi(onDevicesDetected, onMidiInput, onDeviceChange, onNoDevicesFound);

    document.body.addEventListener("selectstart", e => {
        e.preventDefault();
        return false;
    });

    window.addEventListener("contextmenu", e => {
        e.preventDefault();
        return false;
    });

    canvas.addEventListener("mousemove", e => {
        mouse.x = Math.floor(e.clientX - canvas.getBoundingClientRect().left);
        mouse.y = Math.floor(e.clientY - canvas.getBoundingClientRect().top);
    });

    canvas.addEventListener("mousedown", e => {
        switch (e.button) {
            case 0:
                leftMousePressed = true;
                break;
            case 2:
                rightMousePressed = true;
                break;
        }
    });

    canvas.addEventListener("mouseup", e => {
        switch (e.button) {
            case 0:
                leftMousePressed = false;
                break;
            case 2:
                rightMousePressed = false;
                break;
        }

        sendFrameToLaunchpad();
    });

    function goToFrame(id) {
        frame = id;

        if (frame < 0)
            frame = 0;

        if (frame > frames.length - 1)
            frame = frames.length - 1;

        currentFrame = frames[frame];
        previousFrame = frames[frame];

        sendFrameToLaunchpad();
        if (mode == Mode.IDLE)
            updateIdle();
        updateFrameDivs();
    }

    function backwardFrame() {
        goToFrame(parseInt(frame) - 1);
    }

    function forwardFrame() {
        goToFrame(parseInt(frame) + 1);
    }

    function updateFrameDivs() {
        for (let x of document.querySelectorAll(".frame"))
            x.parentElement.removeChild(x);

        for (let i = 0; i < frames.length; i++) {
            let frameDiv = document.createElement("div");
            frameDiv.classList.add("frame");
            frameDiv.setAttribute("index", i);
            
            if (i == frame)
                frameDiv.classList.add("frame-selected");

            frameDiv.addEventListener("click", () => {
                goToFrame(parseInt(frameDiv.getAttribute("index")));
            });

            frameDiv.innerText = i;

            document.querySelector(".frames").appendChild(frameDiv);
        }
    }

    function addFrame() {
        let f = {};

        for (let i = 0; i < 99; i++)
            f[i] = 0;
        
        frames.splice(frame + 1, 0, f);

        forwardFrame();

        updateFrameDivs();
    }

    function duplicateFrame() {
        let f = {};

        for (let i = 0; i < 99; i++)
            f[i] = frames[frame][i];
        
        frames.splice(frame + 1, 0, f);

        forwardFrame();

        updateFrameDivs();
    }

    function removeFrame() {
        if (frames.length > 1)
            frames.splice(frame, 1);
        else {
            clearArray();
            sendFrameToLaunchpad();
        }

        if (frame > frames.length - 1)
            frame = frames.length - 1;
            
        goToFrame(frame);
        updateFrameDivs();
    }

    function aabb(x, y, w, h) {
        return mouse.x > x && mouse.x < x + w &&
               mouse.y > y && mouse.y < y + h;
    }

    //drawing
    function refreshScreen() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        //pads
        for (let y = 0; y < 8; y++) {
            for (let x = 0; x < 8; x++) {
                let xx = 36 + x * 32;
                let yy = 36 + y * 32;
                let w = 24, h = 24;

                ctx.fillStyle = colors[currentFrame[getPadID(x, y)]];
                ctx.fillRect(xx, yy, w, h);

                if (aabb(xx, yy, w, h)) {
                    if (leftMousePressed) {
                        currentFrame[getPadID(x, y)] = currentColor;
                        previousFrame[getPadID(x, y)] = currentColor;
                    }

                    if (rightMousePressed) {
                        currentFrame[getPadID(x, y)] = 0;
                        previousFrame[getPadID(x, y)] = 0;
                    }
                }
            }
        }

        //left round buttons
        for (let i = 0; i < 8; i++) {
            let radius = 8;
            let x = 20 - radius;
            let y = 48 + i * 32 - radius;

            let accessIndex = (8 - i) * 10;

            if (aabb(x, y, radius * 2, radius * 2)) {
                if (leftMousePressed) {
                    currentFrame[accessIndex] = currentColor;
                    previousFrame[accessIndex] = currentColor;
                }
                
                if (rightMousePressed) {
                    currentFrame[accessIndex] = 0;
                    previousFrame[accessIndex] = 0;
                }
            }

            ctx.beginPath();
            ctx.arc(x + radius, y + radius, radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = colors[currentFrame[accessIndex]];
            ctx.fill();
        }

        //top round buttons
        for (let i = 0; i < 8; i++) {
            let radius = 8;
            let x = 48 + i * 32 - radius;
            let y = 20 - radius;

            let accessIndex = 91 + i;

            if (aabb(x, y, radius * 2, radius * 2)) {
                if (leftMousePressed) {
                    currentFrame[accessIndex] = currentColor;
                    previousFrame[accessIndex] = currentColor;
                }

                if (rightMousePressed) {
                    currentFrame[accessIndex] = 0;
                    previousFrame[accessIndex] = 0;
                }
            }

            ctx.beginPath();
            ctx.arc(x + radius, y + radius, radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = colors[currentFrame[accessIndex]];
            ctx.fill();
        }

        //right round buttons
        for (let i = 0; i < 8; i++) {
            let radius = 8;
            let x = 300 - radius;
            let y = 48 + i * 32 - radius;

            let accessIndex = (8 - i).toString() + 9;

            if (aabb(x, y, radius * 2, radius * 2)) {
                if (leftMousePressed) {
                    currentFrame[accessIndex] = currentColor;
                    previousFrame[accessIndex] = currentColor;
                }

                if (rightMousePressed) {
                    currentFrame[accessIndex] = 0;
                    previousFrame[accessIndex] = 0;
                }
            }

            ctx.beginPath();
            ctx.arc(x + radius, y + radius, radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = colors[currentFrame[accessIndex]];
            ctx.fill();
        }

        //bottom round buttons
        for (let i = 0; i < 8; i++) {
            let radius = 8;
            let x = 48 + i * 32 - radius;
            let y = 300 - radius;

            let accessIndex = 1 + i;

            if (aabb(x, y, radius * 2, radius * 2)) {
                if (leftMousePressed) {
                    currentFrame[accessIndex] = currentColor;
                    previousFrame[accessIndex] = currentColor;
                }

                if (rightMousePressed) {
                    currentFrame[accessIndex] = 0;
                    previousFrame[accessIndex] = 0;
                }
            }

            ctx.beginPath();
            ctx.arc(x + radius, y + radius, radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = colors[currentFrame[accessIndex]];
            ctx.fill();
        }

        requestAnimationFrame(refreshScreen);
    }

    window.addEventListener("resize", () => {
        requestAnimationFrame(refreshScreen);
    });

    clearArray();

    refreshScreen();

    function timeChange() {
        let time = document.querySelector("#time");
        let hour = new Date().getHours();
        let greeter = "";

        if (hour >= 6 && hour < 12)
            greeter = "morning";

        if (hour >= 12 && hour < 19)
            greeter = "afternoon";

        if (hour >= 19 || hour < 6)
            greeter = "evening";

        time.innerText = `Good ${greeter}`;
    }

    setInterval(timeChange, 200);
    timeChange();

    window.addEventListener("beforeunload", e => {
        clearLaunchpad();
        for (let x of midiPorts)
            x.close();
    }, false);

    resetCanvasButton.addEventListener("click", () => {
        reset();
    });

    addFrameButton.addEventListener("click", addFrame);
    duplicateFrameButton.addEventListener("click", duplicateFrame);
    removeFrameButton.addEventListener("click", removeFrame);
    playAnimationButton.addEventListener("click", () => {
        animation.data = frames;
        playAnimation(animation, () => {
            sendFrameToLaunchpad();
            if (mode == Mode.IDLE)
                updateIdle();
        });
    });

    for (let x of document.querySelectorAll(".color")) {
        x.style.backgroundColor = colors[x.getAttribute("c")];
        x.addEventListener("click", () => {
            currentColor = x.getAttribute("c");
            for (let y of document.querySelectorAll(".color")) {
                y.classList.remove("color-selected");
            }
            x.classList.add("color-selected");
            if (mode == Mode.IDLE)
                updateIdle();
        });
    }

    document.querySelectorAll(".color")[0].classList.add("color-selected");
    document.querySelectorAll(".frame")[0].classList.add("frame-selected");

    frames.push(currentFrame);

    function getCurrentColorIndex() {
        let colors = document.querySelectorAll(".color");
        let index = -1;

        for (let i = 0; i < colors.length; i++) {
            if (colors[i].classList.contains("color-selected"))
                index = i;
        }

        return index;
    }

    function getColorFromIndex(id) {
        let colors = document.querySelectorAll(".color");
        return colors[id].getAttribute("c");
    }

    function setColor(id) {
        let colors = document.querySelectorAll(".color");

        if (id < 0)
            id = colors.length - 1;

        if (id > colors.length - 1)
            id = 0;

        currentColor = getColorFromIndex(id);

        for (let x of document.querySelectorAll(".color"))
            x.classList.remove("color-selected");
            
        colors[id].classList.add("color-selected");

        if (mode == Mode.IDLE)
            updateIdle();
    }

    function changeMode(m) {
        switch (m) {
            case Mode.IDLE:
                sendFrameToLaunchpad();
                updateIdle();
                mode = m;
                break;
            case Mode.DRAW:
            case Mode.TOGGLE:
                clearLaunchpad();
                sendFrameToLaunchpad();
                mode = m;
                break;
        }
    }

    colorPicker.addEventListener("wheel", e => {
        e.preventDefault();

        let index = getCurrentColorIndex();

        if (e.deltaY > 0)
            index--;
        else if (e.deltaY < 0)
            index++;

        setColor(index);
    });

    window.addEventListener("keydown", e => {
        switch (e.code) {
            case "ArrowLeft":
                backwardFrame();
                break;
            case "ArrowRight":
                forwardFrame();
                break;
            case "KeyC":
                clearArray();
                sendFrameToLaunchpad();
                break;
            case "KeyD":
                duplicateFrame();
                break;
            case "KeyN":
                addFrame();
                break;
            case "KeyR":
                removeFrame();
                break;
            case "KeyP":
                animation.data = frames;
                playAnimation(animation, () => {
                    sendFrameToLaunchpad();
                    if (mode == Mode.IDLE)
                        updateIdle();
                });
                break;
            case "KeyL":
                console.log(frames);
                break;
            case "KeyM":
                if (mode == Mode.IDLE) {
                    changeMode(Mode.DRAW);
                    break;
                } else if (mode == Mode.DRAW)
                    changeMode(Mode.IDLE);
                break;
        }
    });
});