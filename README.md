# 0xFF

![logo](res/logo3.png)

![screenshot](res/screenshot.png)

A web-based tool for creating the infamous "LAUNCHPAD COVERS" with Novation's Launchpad Pro.

This repository features both the tool and some helper functions along with a color table (based on Novation's Launchpad Pro Programming Reference Manual).

Here's a video demonstrating the current StateOfAffairs™:

![demo](res/demo.mp4)

More to come.